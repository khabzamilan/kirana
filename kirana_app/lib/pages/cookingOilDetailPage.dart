// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kirana_app/model/fruits.dart';
import 'package:kirana_app/model/oil.dart';
import 'package:kirana_app/util/cards.dart';
import 'package:kirana_app/constants/app_colors.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class CookingOilModelsArguments {
  final ItemOil? itemOil;
  List<Item> list;

  CookingOilModelsArguments({this.itemOil, required this.list});
}

class CookingOilDetailPage extends StatefulWidget {
  CookingOilModelsArguments? cookingOilModelsArguments;

  CookingOilDetailPage({super.key, this.cookingOilModelsArguments});

  @override
  State<CookingOilDetailPage> createState() => _CookingOilDetailPageState();
}

class _CookingOilDetailPageState extends State<CookingOilDetailPage> {
  final PageController _controller = PageController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 70,
          elevation: 0,
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Center(
                child: FaIcon(
              FontAwesomeIcons.arrowLeft,
              color: AppColors.Primary,
            )),
          ),
          actions: [
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: Container(
                  padding: const EdgeInsets.all(7),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: AppColors.Primary.withOpacity(0.1)),
                  child: const FaIcon(
                    FontAwesomeIcons.heart,
                    color: AppColors.Primary,
                    size: 24,
                  ),
                ),
              ),
            )
          ],
          backgroundColor: Colors.transparent,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: const EdgeInsets.symmetric(vertical: 5, horizontal: 20),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 300,
                  width: 350,
                  child: Stack(
                    children: [
                      PageView(
                        controller: _controller,
                        children: [
                          SizedBox(
                            child: Image.asset(
                              widget.cookingOilModelsArguments?.itemOil
                                      ?.image ??
                                  '',
                              fit: BoxFit.contain,
                            ),
                          ),
                          SizedBox(
                            child: Image.asset(
                              widget.cookingOilModelsArguments?.itemOil
                                      ?.image ??
                                  '',
                              fit: BoxFit.contain,
                            ),
                          ),
                          SizedBox(
                            child: Image.asset(
                              widget.cookingOilModelsArguments?.itemOil
                                      ?.image ??
                                  '',
                              fit: BoxFit.contain,
                            ),
                          )
                        ],
                      ),
                      Container(
                        alignment: const Alignment(-0, 1),
                        child: SmoothPageIndicator(
                          controller: _controller,
                          count: 3,
                          effect: const ExpandingDotsEffect(
                            dotColor: AppColors.Grey,
                            activeDotColor: AppColors.Primary,
                            radius: 16,
                            dotHeight: 6,
                            dotWidth: 6,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                const SizedBox(height: 20),
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text(
                    widget.cookingOilModelsArguments?.itemOil?.name ?? '',
                    style: const TextStyle(
                        fontWeight: FontWeight.bold, fontSize: 18),
                  ),
                  const SizedBox(height: 10),
                  RichText(
                      text: TextSpan(
                          text: widget
                              .cookingOilModelsArguments?.itemOil?.quantity,
                          style: const TextStyle(
                              fontSize: 14,
                              color: AppColors.Grey,
                              fontWeight: FontWeight.w500),
                          children: const <TextSpan>[
                        TextSpan(
                            text: ", Price",
                            style:
                                TextStyle(fontSize: 14, color: AppColors.Grey)),
                      ])),
                  const SizedBox(height: 10),
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      RichText(
                          text: TextSpan(
                              text: "Rs ",
                              style: const TextStyle(
                                  fontSize: 18,
                                  color: AppColors.Black,
                                  fontWeight: FontWeight.bold),
                              children: <TextSpan>[
                            TextSpan(
                              text: widget
                                  .cookingOilModelsArguments?.itemOil?.price
                                  .toString(),
                              // style: TextStyle(fontSize: 18, color: Pallet.Grey),
                            ),
                            const TextSpan(
                                text: " (",
                                style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w400,
                                    decoration: TextDecoration.lineThrough)),
                            const TextSpan(
                                text: "Nrs ",
                                style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w400,
                                    decoration: TextDecoration.lineThrough)),
                            TextSpan(
                                text: widget.cookingOilModelsArguments?.itemOil
                                    ?.oldPrice
                                    .toString(),
                                style: const TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w400,
                                    decoration: TextDecoration.lineThrough)
                                // style: TextStyle(fontSize: 18, color: Pallet.Grey),
                                ),
                            const TextSpan(
                                text: ")",
                                style: TextStyle(
                                    fontSize: 10,
                                    fontWeight: FontWeight.w400,
                                    decoration: TextDecoration.lineThrough)),
                          ])),
                      const SizedBox(width: 5),
                      Container(
                        alignment: Alignment.center,
                        width: 45, height: 15,
                        // padding: const EdgeInsets.symmetric(
                        //     vertical: 0, horizontal: 5),
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(2)),
                        child: const Text(
                          "15% OFF",
                          style: TextStyle(fontSize: 10, color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                  const SizedBox(height: 20),
                  const Text(
                    "Product Description",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
                  const SizedBox(height: 10),
                  Text(
                    widget.cookingOilModelsArguments?.itemOil?.desc ?? '',
                    style: const TextStyle(fontSize: 12, color: AppColors.Grey),
                  ),
                  const SizedBox(height: 20),
                  const Text(
                    "Ratings & Reviews (0)",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
                  const SizedBox(height: 10),
                  const Text(
                    "No Reviews Yet.",
                    style: TextStyle(fontSize: 12, color: AppColors.Grey),
                  ),
                  const SizedBox(height: 20),
                  const Text(
                    "Products by Seller",
                    style: TextStyle(fontWeight: FontWeight.w600, fontSize: 16),
                  ),
                  const SizedBox(height: 10),
                  Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          blurRadius: 18,
                          offset: const Offset(4, 4),
                          color: AppColors.Black.withOpacity(0.1))
                    ]),
                    height: 266,
                    child: ListView.builder(
                      itemCount: widget.cookingOilModelsArguments?.list.length,
                      itemBuilder: (context, index) {
                        var list =
                            widget.cookingOilModelsArguments?.list[index];
                        return Row(
                          children: [
                            Cards(
                                quantity: list?.quantity ?? '',
                                price: list?.price.toString() ?? '',
                                imagePath: list?.image,
                                title: list?.name ?? '',
                                oldPrice: list?.oldPrice.toString() ?? ''),
                            const SizedBox(width: 15)
                          ],
                        );
                      },
                      scrollDirection: Axis.horizontal,
                    ),
                  ),
                  const SizedBox(
                    height: 75,
                  )
                ]),
              ],
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(0)),
          child: Container(
            padding: const EdgeInsets.all(10.0),
            // ignore: prefer_const_constructors
            decoration: BoxDecoration(color: Colors.white, boxShadow: const [
              BoxShadow(offset: Offset(0, 8), blurRadius: 16, spreadRadius: 0)
            ]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const FaIcon(FontAwesomeIcons.minus),
                Container(
                  width: 47,
                  height: 34,
                  // padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      border: Border.all(width: 0.5),
                      borderRadius: BorderRadius.circular(6)),
                  child: const Center(
                    child: Text(
                      // textAlign: TextAlign.center,
                      "1",
                      style:
                          TextStyle(fontSize: 20, fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
                const FaIcon(FontAwesomeIcons.plus),
                Container(
                  width: 183,
                  height: 42,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8),
                    // color: Pallet.Black,
                  ),
                  child: FloatingActionButton.extended(
                      backgroundColor: AppColors.Primary,
                      shape: const RoundedRectangleBorder(
                          borderRadius: BorderRadius.all(Radius.circular(8))),
                      onPressed: () {},
                      label: const Text(
                        "Add to Cart",
                        style: TextStyle(
                            fontSize: 16, fontWeight: FontWeight.w600),
                      )),
                )
              ],
            ),
          ),
        ));
  }
}
