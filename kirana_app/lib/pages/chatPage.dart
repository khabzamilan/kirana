// ignore_for_file: avoid_print

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
// import 'package:http/http.dart' as http;
import 'package:kirana_app/model/albumModel.dart';
import '../constants/app_colors.dart';

class ChatPage extends StatefulWidget {
  const ChatPage({super.key});

  @override
  State<ChatPage> createState() => _ChatPageState();
}

class _ChatPageState extends State<ChatPage> {
  final dio = Dio();
  List<Album> list = [];
  final _baseUrl = 'https://jsonplaceholder.typicode.com/posts';
  int _page = 1;
  final int _limit = 20;
  bool _hasNextPage = true;
  bool _isFirstLoadRunning = false;
  bool _isLoadMoreRunning = false;

  // final Product = product.fetchAll();
  // @override
  // void initState() {
  //   super.initState();
  //   // loadData();
  //   fetchData();
  // }

  // Future<void> fetchData() async {
  //   final response =
  //       await dio.get('https://jsonplaceholder.typicode.com/posts');
  //   if (response.statusCode == 200) {
  // setState(() {
  //       var decodedAlbumData = jsonDecode(response.body);

  //       AlbumModel.albums = List.from(decodedAlbumData)
  //           .map<Album>((album) => Album.fromJson(album))
  //           .toList();
  //     });
  //   } else {
  //     throw Exception('Failed to load data');
  //   }
  // }

  void _firstLoad() async {
    setState(() {
      _isFirstLoadRunning = true;
    });
    try {
      final response = await dio.get("$_baseUrl?_page=$_page&_limit=$_limit");
      setState(() {
        response.data.forEach((e) {
          list.add(Album.fromJson(e));
        });
        _isFirstLoadRunning = false;
      });
    } catch (err) {
      setState(() {
        _isFirstLoadRunning = false;
      });
    }
  }

  void _loadMore() async {
    if (_hasNextPage == true &&
        _isFirstLoadRunning == false &&
        _isLoadMoreRunning == false &&
        _controller.position.extentAfter < 300) {
      setState(() {
        _isLoadMoreRunning = true;
      });
      _page += 1;
      try {
        final response = await dio.get("$_baseUrl?_page=$_page&_limit=$_limit");

        if (response.data.isNotEmpty) {
          setState(() {
            response.data.forEach((e) {
              list.add(Album.fromJson(e));
            });
            print(list.length);
          });
        } else {
          setState(() {
            _hasNextPage = false;
          });
        }
      } catch (err) {
        print(err.toString());
      }
      setState(() {
        _isLoadMoreRunning = false;
      });
    }
  }

  late ScrollController _controller;

  @override
  void initState() {
    super.initState();
    _firstLoad();
    _controller = ScrollController()..addListener(_loadMore);
  }

  @override
  void dispose() {
    _controller.removeListener(_loadMore);
    super.dispose();
  }

  // Future<void> _makeRequest() async {
  //   try {
  //     final response =
  //         await dio.get('https://jsonplaceholder.typicode.com/posts');

  //     setState(() {
  //       // _responseText = response.data.toString();
  //       // var decodedAlbumData = jsonDecode(response.data);
  //       response.data.forEach((e) {
  //         list.add(Album.fromJson(e));
  //       });

  //       // AlbumModel.albums = List.from(response.data)
  //       //     .map<Album>((album) => Album.fromJson(album))
  //       //     .toList();
  //     });
  //   } on DioError catch (e) {
  //     if (e.response != null) {
  //       print('Dio error!');
  //       print('STATUS: ${e.response?.statusCode}');
  //       print('DATA: ${e.response?.data}');
  //       print('HEADERS: ${e.response?.headers}');
  //     } else {
  //       print('Error sending request!');
  //       print(e.message);
  //     }
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 70.h,
          elevation: 0,
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Center(
                child: FaIcon(
              FontAwesomeIcons.arrowLeft,
              color: AppColors.Primary,
            )),
          ),
          title: Center(
            child: Text(
              "Chat",
              style: TextStyle(
                color: AppColors.Black,
                fontSize: 16.sp,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
          backgroundColor: Colors.transparent,
        ),
        body: _isFirstLoadRunning
            ? const Center(
                child: CircularProgressIndicator(),
              )
            : Column(
                children: [
                  Expanded(
                    child: ListView.builder(
                      controller: _controller,
                      itemCount: list.length,
                      itemBuilder: (_, index) => Card(
                        margin: EdgeInsets.symmetric(
                            vertical: 8.h, horizontal: 10.w),
                        child: ListTile(
                          title: Row(
                            children: [
                              // Text(_post[index]['id'].toString()),
                              Text(list[index].id.toString()),

                              // Text(list[index].title),
                            ],
                          ),
                          subtitle:
                              // Text(_post[index]['body']
                              // ),
                              Text(list[index].body),
                        ),
                      ),
                    ),
                  ),
                  //when the _loadMore function is running
                  if (_isLoadMoreRunning == true)
                    Padding(
                      padding: const EdgeInsets.only(top: 10, bottom: 40).h,
                      child: const Center(
                        child: CircularProgressIndicator(),
                      ),
                    ),

                  //when nothing else to load

                  if (_hasNextPage == false)
                    Container(
                      padding: const EdgeInsets.only(top: 30, bottom: 40).h,
                      color: Colors.green[200],
                      child: const Center(
                        child: Text('You have fetched all of the content'),
                      ),
                    ),
                ],
              )

        // ListView.builder(
        //     itemCount: list.length,
        //     itemBuilder: (BuildContext context, int index) {
        //       // Album albumModel = AlbumModel.albums[index];
        //       return Card(
        //         child: ListTile(
        //           title: Text(list[index].title

        //               // overflow: TextOverflow.ellipsis,
        //               ),
        //           subtitle: Text(list[index].body),
        //         ),
        //       );
        //     }),
        );
  }
}
