import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kirana_app/pages/imagepicker.dart';

import '../constants/app_colors.dart';

class AllFruitPage extends StatefulWidget {
  const AllFruitPage({super.key});

  @override
  State<AllFruitPage> createState() => _AllFruitPageState();
}

class _AllFruitPageState extends State<AllFruitPage> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 70,
          elevation: 0,
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Center(
                child: FaIcon(
              FontAwesomeIcons.arrowLeft,
              color: AppColors.Primary,
            )),
          ),
          title: const Center(
            child: Text(
              // textAlign: TextAlign.center,
              "Fruits",
              style: TextStyle(
                color: AppColors.Black,
                fontSize: 16,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          actions: [
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(right: 20.0),
                child: Container(
                  padding: const EdgeInsets.all(7),
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50),
                      color: AppColors.Primary.withOpacity(0.1)),
                  child: const FaIcon(
                    FontAwesomeIcons.plus,
                    color: AppColors.Primary,
                    size: 24,
                  ),
                ),
              ),
            )
          ],
          backgroundColor: Colors.transparent,
        ),
        body: Container(
          padding: const EdgeInsets.symmetric(vertical: 15),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: 10),
            child: Column(children: [
              TextFormField(
                decoration: InputDecoration(
                    filled: true,
                    enabledBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.r),
                        borderSide: BorderSide(color: AppColors.Grey)),
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(8.r),
                        borderSide: BorderSide(color: AppColors.Grey)),
                    hintText: "What are you looking for?",
                    hintStyle: Theme.of(context).textTheme.labelMedium),
              ),
              ImagePickerWidgetScreen(onImageSelected: (File file) {})
            ]),
          ),
        ));
  }
}
