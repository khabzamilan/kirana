import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kirana_app/model/shoppingListModel.dart';
import 'package:kirana_app/util/shoppingList.dart';

import '../constants/app_colors.dart';

class ListPage extends StatefulWidget {
  const ListPage({super.key});

  @override
  State<ListPage> createState() => _ListPageState();
}

class _ListPageState extends State<ListPage> {
  final itemList = ShoppingModel.itemList();
  List<ShoppingList> _foundShoppingList = [];

  final _shoppingListController = TextEditingController();

  @override
  void initState() {}

  _handleShoppingListChange(ShoppingModel shoppingModel) {
    setState(() {
      shoppingModel.isDone = !shoppingModel.isDone;
    });
  }

  _deleteShoppingListItem(String id) {
    setState(() {
      itemList.removeWhere((item) => item.id == id);
    });
  }

  _addShoppingList(String shoppingList) {
    setState(() {
      itemList.add(ShoppingModel(
        id: DateTime.now().microsecond.toString(),
        shoppingList: shoppingList,
      ));
    });
    _shoppingListController.clear();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Center(
              child: FaIcon(
            FontAwesomeIcons.arrowLeft,
            color: AppColors.Primary,
          )),
        ),
        title: const Center(
          child: Text(
            // textAlign: TextAlign.center,
            "Shopping Lists",
            style: TextStyle(
              color: AppColors.Black,
              fontSize: 16,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        backgroundColor: Colors.transparent,
      ),
      body: Container(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: SingleChildScrollView(
          child: Column(children: [
            Container(
              padding: const EdgeInsets.symmetric(horizontal: 15),
              decoration: BoxDecoration(
                  boxShadow: [
                    BoxShadow(
                        blurRadius: 18,
                        offset: const Offset(4, 4),
                        color: AppColors.Black.withOpacity(0.1))
                  ],
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(8),
                  border: Border.all(
                      width: 0.5, color: AppColors.Primary.withOpacity(0.2))),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  SizedBox(
                    width: 200,
                    // ignore: prefer_const_constructors
                    child: TextField(
                      controller: _shoppingListController,
                      decoration: const InputDecoration(
                          border: InputBorder.none,
                          hintText: 'New List',
                          hintStyle: TextStyle(
                            color: AppColors.Grey,
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          )),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      _addShoppingList(_shoppingListController.text);
                    },
                    child: Row(
                      // ignore: prefer_const_literals_to_create_immutables
                      children: [
                        const FaIcon(
                          FontAwesomeIcons.plus,
                          color: AppColors.Grey,
                          size: 18,
                        ),
                        const SizedBox(
                          width: 5,
                        ),
                        const Text(
                          "Add",
                          style: TextStyle(
                            color: AppColors.Grey,
                            fontSize: 16,
                            fontWeight: FontWeight.w500,
                          ),
                        )
                      ],
                    ),
                  )
                ],
              ),
            ),
            const SizedBox(height: 15),
            for (ShoppingModel shoppingModel in itemList)
              ShoppingList(
                shoppingModel: shoppingModel,
                onDeleteItem: _deleteShoppingListItem,
                onToDoChanged: _handleShoppingListChange,
              ),
          ]),
        ),
      ),
    );
  }
}
