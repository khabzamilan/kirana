import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kirana_app/constants/asset_source.dart';
import 'package:kirana_app/util/shoppingList.dart';

import '../constants/app_colors.dart';

class ProfilePage extends StatelessWidget {
  const ProfilePage({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        // appBar: AppBar(
        //   toolbarHeight: 70,
        //   elevation: 0,
        //   leading: GestureDetector(
        //     onTap: () {
        //       Navigator.pop(context);
        //     },
        //     child: const Center(
        //         child: FaIcon(
        //       FontAwesomeIcons.arrowLeft,
        //       color: AppColors.Primary,
        //     )),
        //   ),
        //   title: const Center(
        //     child: Text(
        //       // textAlign: TextAlign.center,
        //       "Profile",
        //       style: TextStyle(
        //         color: AppColors.Black,
        //         fontSize: 16,
        //         fontWeight: FontWeight.bold,
        //       ),
        //     ),
        //   ),
        //   backgroundColor: Colors.transparent,
        // ),
        body: SafeArea(
      child: SingleChildScrollView(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: 20, horizontal: 20),
          child: Column(
            children: [
              Row(
                children: [
                  Container(
                      decoration:
                          BoxDecoration(borderRadius: BorderRadius.circular(8)),
                      width: 54,
                      height: 54,
                      child: Image.asset(
                        AssetSource.appLogo,
                        fit: BoxFit.cover,
                      )),
                  Text(
                    "KIRANA",
                    style: TextStyle(fontSize: 20, fontWeight: FontWeight.bold),
                  )
                ],
              ),
            ],
          ),
        ),
      ),
    ));
  }
}
