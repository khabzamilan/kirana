import 'dart:io';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:image_picker/image_picker.dart';
import 'package:kirana_app/constants/asset_source.dart';

class ImagePickerWidgetScreen extends StatefulWidget {
  final Function(File) onImageSelected;
  const ImagePickerWidgetScreen({super.key, required this.onImageSelected});

  @override
  State<ImagePickerWidgetScreen> createState() =>
      _ImagePickerWidgetScreenState();
}

class _ImagePickerWidgetScreenState extends State<ImagePickerWidgetScreen> {
  // late File _imageFile;
  // late final ImagePicker _picker = ImagePicker();
  late File _imageFile = File(AssetSource.appleImage);

  Future<void> _pickImages(ImageSource source) async {
    final pickedImages = await ImagePicker().pickImage(source: source);
    if (pickedImages != null) {
      setState(() {
        _imageFile = File(pickedImages.path);
      });
      widget.onImageSelected(_imageFile);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        GestureDetector(
          onTap: () {
            showDialog(
              context: context,
              builder: (BuildContext context) {
                return AlertDialog(
                  title: const Text('Select an image source'),
                  actions: [
                    TextButton(
                      onPressed: () => Navigator.of(context).pop(),
                      child: Text('Cancel'),
                    ),
                    TextButton(
                      onPressed: () {
                        _pickImages(ImageSource.gallery);
                        Navigator.of(context).pop();
                      },
                      child: Text('Gallery'),
                    ),
                    TextButton(
                      onPressed: () {
                        _pickImages(ImageSource.camera);
                        Navigator.of(context).pop();
                      },
                      child: Text('Camera'),
                    ),
                  ],
                );
              },
            );
          },
          child: Container(
            width: 150.0,
            height: 150.0,
            decoration: BoxDecoration(
              shape: BoxShape.circle,
              image: DecorationImage(
                fit: BoxFit.cover,
                image: _imageFile != null
                    ? FileImage(_imageFile)
                    : const AssetImage(AssetSource.appleImage)
                        as ImageProvider<Object>,
              ),
              border: Border.all(
                color: Colors.grey,
                width: 2.0,
              ),
            ),
          ),
        ),
        SizedBox(height: 10.0.h),
        Text(
          'Tap the image to select photos',
          style: TextStyle(
            fontSize: 16.0.sp,
            fontWeight: FontWeight.bold,
          ),
        ),
      ],
    );
  }
}
