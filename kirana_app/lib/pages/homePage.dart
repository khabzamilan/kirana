// ignore_for_file: file_names, deprecated_member_use

import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kirana_app/constants/asset_source.dart';
import 'package:kirana_app/constants/custom_icon.dart';
import 'package:kirana_app/model/fruits.dart';
import 'package:kirana_app/model/oil.dart';
import 'package:kirana_app/pages/cookingOilDetailPage.dart';
import 'package:kirana_app/pages/fruitsDetailPage.dart';
// import 'package:kirana_app/pages/fruitsDetailPage.dart';
import 'package:kirana_app/util/cards.dart';
import 'dart:convert';
import 'package:kirana_app/util/categories.dart';
import 'package:http/http.dart' as http;

import 'package:kirana_app/constants/app_colors.dart';
import 'package:kirana_app/util/routes.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class HomePage extends StatefulWidget {
  const HomePage({super.key});

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  final PageController _controller = PageController();
  // final Product = Product.fetchAll();
  @override
  void initState() {
    super.initState();
    loadData();
    // fetchData();
  }

  void loadData() async {
    // await Future.delayed(const Duration(seconds: 5));
    final fruitJson = await rootBundle.loadString("asset/files/fruits.json");
    final cookingOilJson =
        await rootBundle.loadString("asset/files/cookingOil.json");
    final decodedFruitsData = jsonDecode(fruitJson);
    final decodedCookingOilData = jsonDecode(cookingOilJson);
//promotion json load
    final promotion = await rootBundle.loadString('asset/files/promotion.json');
    //decoding
    // final decodedPromotionData   = jsonDecode(promotion);
    // var PromotionData = decodedPromotionData["promotion"];

    var fruitsData = decodedFruitsData["fruits"];
    var cookingOilData = decodedCookingOilData["cooking oil"];

    FruitsModel.items =
        List.from(fruitsData).map<Item>((item) => Item.fromMap(item)).toList();
    CookingOilModel.itemsoil = List.from(cookingOilData)
        .map<ItemOil>((item) => ItemOil.fromMap(item))
        .toList();

    setState(() {});
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 0,
        leadingWidth: 0,
        backgroundColor: Colors.transparent,
        title: Column(
          children: [
            Row(
              children: [
                Text(
                  // textAlign: TextAlign.center,
                  'Delivering Location',
                  style: TextStyle(color: AppColors.Grey, fontSize: 13.sp),
                ),
                SizedBox(width: 5.w),
                GestureDetector(
                  onTap: () {},
                  child: FaIcon(
                    size: 16.w,
                    FontAwesomeIcons.angleDown,
                    color: AppColors.Grey,
                  ),
                  // Image.asset('asset/icons/Vector.png'),
                )
              ],
            ),
            SizedBox(height: 2.h),
            Row(
              children: [
                GestureDetector(
                  onTap: () {},
                  child: FaIcon(
                    size: 16.w,
                    FontAwesomeIcons.locationDot,
                    color: AppColors.Black,
                  ),
                  // Image.asset('asset/icons/Frame.png'),
                ),
                SizedBox(width: 5.w),
                Text(
                  'Kathmandu, Nepal',
                  style: TextStyle(color: AppColors.Black, fontSize: 13.sp),
                ),
              ],
            ),
          ],
        ),
        actions: [
          Align(
            alignment: Alignment.center,
            child: Padding(
              padding: const EdgeInsets.only(right: 20.0).r,
              child: FaIcon(
                FontAwesomeIcons.solidBell,
                color: AppColors.Black,
                size: 24.w,
              ),
            ),
          )
        ],
        // actions: [Image.asset('asset/icons/bell-fill.png')],
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 20.w),
            child: Column(children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: [
                  Container(
                    padding: EdgeInsets.symmetric(horizontal: 10.w),
                    width: 0.73.sw,
                    height: 48.h,
                    decoration: BoxDecoration(
                        color: AppColors.Primary.withOpacity(0.15),
                        borderRadius: BorderRadius.circular(8).w),
                    child: Row(
                      // ignore: prefer_const_literals_to_create_immutables
                      children: [
                        FaIcon(
                          FontAwesomeIcons.magnifyingGlass,
                          size: 20.w,
                        ),
                        SizedBox(width: 5.w),
                        Text(
                          'What are you looking for?',
                          style: TextStyle(
                              color: AppColors.Black, fontSize: 13.sp),
                        ),
                      ],
                    ),
                  ),
                  Container(
                    height: 48.h,
                    width: 48.w,
                    decoration: BoxDecoration(
                        color: AppColors.Primary,
                        borderRadius: BorderRadius.circular(8).w),
                    child: SizedBox(
                      width: 24.w,
                      height: 24.h,
                      child: SvgPicture.asset(
                        CustomIcon.filterIcon,
                        fit: BoxFit.none,
                      ),
                    ),
                  )
                ],
              ),
              SizedBox(height: 5.h),
              ClipRRect(
                child: Image.asset(
                  AssetSource.mainBanner,
                  fit: BoxFit.contain,
                  width: 1.sw,
                ),
              ),
              SizedBox(height: 5.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  Text(
                    'Categories',
                    style: TextStyle(
                        color: AppColors.Black,
                        fontSize: 20.sp,
                        fontWeight: FontWeight.w600),
                  ),
                  GestureDetector(
                    child: Text(
                      'View All',
                      style: TextStyle(
                          color: AppColors.Primary,
                          fontSize: 13.sp,
                          fontWeight: FontWeight.normal),
                    ),
                    onTap: () {
                      Navigator.pushNamed(context, Routes.categoriesRoute);
                    },
                  )
                ],
              ),
              SizedBox(height: 15.h),
              SizedBox(
                height: 100.h,
                child: ListView(
                  scrollDirection: Axis.horizontal,
                  // ignore: prefer_const_literals_to_create_immutables
                  children: [
                    GestureDetector(
                      onTap: () {
                        Navigator.pushNamed(context, Routes.beerPage);
                      },
                      child: const CategoriesCard(
                        imagePath: AssetSource.vegetablesFruits,
                        categoryName: 'Vegetables \n& Fruits',
                      ),
                    ),
                    SizedBox(width: 15.w),
                    const CategoriesCard(
                      imagePath: AssetSource.bakery,
                      categoryName: 'Bakery & \nDairy',
                    ),
                    SizedBox(width: 15.w),
                    const CategoriesCard(
                      imagePath: AssetSource.dairyEggs,
                      categoryName: 'Dairy & Eggs',
                    ),
                    SizedBox(width: 15.w),
                    const CategoriesCard(
                      imagePath: AssetSource.meatFish,
                      categoryName: 'Meat & Fish',
                    ),
                    SizedBox(width: 15.w),
                    const CategoriesCard(
                      imagePath: AssetSource.beautyProducts,
                      categoryName: 'Beauty Products',
                    ),
                    SizedBox(width: 15.w),
                  ],
                ),
              ),
              SizedBox(height: 15.h),
              SizedBox(
                width: 1.sw,
                height: 120.h,
                child: Stack(
                  children: [
                    PageView(
                      controller: _controller,
                      children: [
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8).w),
                          child: SizedBox(
                            child: Image.asset(
                              AssetSource.promotionalBanner1,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8).w),
                          child: SizedBox(
                            child: Image.asset(
                              AssetSource.promotionalBanner2,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                        Container(
                          decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8).w),
                          child: SizedBox(
                            child: Image.asset(
                              AssetSource.promotionalBanner3,
                              fit: BoxFit.cover,
                            ),
                          ),
                        ),
                      ],
                    ),
                    Container(
                      alignment: const Alignment(-0, 0.85),
                      child: SmoothPageIndicator(
                        controller: _controller,
                        count: 3,
                        effect: ExpandingDotsEffect(
                          dotColor: AppColors.Grey,
                          activeDotColor: Colors.green,
                          radius: 16.r,
                          dotHeight: 9.h,
                          dotWidth: 9.w,
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 15.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  Text(
                    'Fruits',
                    style: TextStyle(
                        color: AppColors.Black,
                        fontSize: 20.sp,
                        fontWeight: FontWeight.w600),
                  ),
                  GestureDetector(
                    onTap: () {
                      Navigator.pushNamed(context, Routes.allFruitsRoute);
                    },
                    child: Text(
                      'View All',
                      style: TextStyle(
                          color: AppColors.Primary,
                          fontSize: 13.sp,
                          fontWeight: FontWeight.normal),
                    ),
                  )
                ],
              ),
              SizedBox(height: 15.h),
              Container(
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                        blurRadius: 18,
                        offset: const Offset(4, 4),
                        color: AppColors.Black.withOpacity(0.1))
                  ]),
                  height: 267.h,
                  child: (FruitsModel.items != null &&
                          FruitsModel.items.isNotEmpty)
                      ? ListView.builder(
                          itemCount: FruitsModel.items.length,
                          itemBuilder: (context, index) {
                            Item model = FruitsModel.items[index];
                            return Row(
                              children: [
                                GestureDetector(
                                  child: Cards(
                                    price: model.price.toString(),
                                    imagePath: model.image,
                                    count: model.count,
                                    title: model.name,
                                    quantity: model.quantity ?? '',
                                    oldPrice: model.oldPrice.toString(),
                                    // quantity: modleOil.quantity,
                                  ),
                                  onTap: () {
                                    Navigator.pushNamed(
                                        context, Routes.fruitdetailPage,
                                        arguments: ModelsArguments(
                                          item: model,
                                          list: CookingOilModel.itemsoil,
                                        ));
                                  },
                                ),
                                SizedBox(width: 15.w),
                              ],
                            );
                          },
                          scrollDirection: Axis.horizontal,
                        )
                      : const Center(
                          child: CircularProgressIndicator(),
                        )),
              SizedBox(height: 20.h),
              Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  Text(
                    'Cooking oil Deals',
                    style: TextStyle(
                        color: AppColors.Black,
                        fontSize: 20.sp,
                        fontWeight: FontWeight.w600),
                  ),
                  Text(
                    'View All',
                    style: TextStyle(
                        color: AppColors.Primary,
                        fontSize: 13.sp,
                        fontWeight: FontWeight.normal),
                  )
                ],
              ),
              SizedBox(height: 15.h),
              Container(
                  decoration: BoxDecoration(boxShadow: [
                    BoxShadow(
                        blurRadius: 18,
                        offset: const Offset(4, 4),
                        color: AppColors.Black.withOpacity(0.1))
                  ]),
                  height: 267.h,
                  child: (CookingOilModel.itemsoil != null &&
                          CookingOilModel.itemsoil.isNotEmpty)
                      ? ListView.builder(
                          itemCount: CookingOilModel.itemsoil.length,
                          itemBuilder: (context, index) {
                            ItemOil oilmodel = CookingOilModel.itemsoil[index];
                            return Row(
                              children: [
                                GestureDetector(
                                  child: Cards(
                                    price: oilmodel.price.toString(),
                                    imagePath: oilmodel.image,
                                    title: oilmodel.name,
                                    quantity: oilmodel.quantity,
                                    oldPrice: oilmodel.oldPrice.toString(),
                                  ),
                                  onTap: () {
                                    Navigator.pushNamed(
                                        context, Routes.cookingoildetailPage,
                                        arguments: CookingOilModelsArguments(
                                          itemOil: oilmodel,
                                          list: FruitsModel.items,
                                        ));
                                  },
                                ),
                                SizedBox(width: 15.w),
                              ],
                            );
                          },
                          scrollDirection: Axis.horizontal,
                        )
                      : const Center(
                          child: CircularProgressIndicator(),
                        )),
              SizedBox(height: 15.w),
            ]),
          ),
        ),
      ),
    );
  }
}
