// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kirana_app/onBordingScreens/onBoardingPage1.dart';
import 'package:kirana_app/onBordingScreens/onBoardingPage2.dart';
import 'package:kirana_app/onBordingScreens/onBoardingPage3.dart';
import 'package:kirana_app/constants/app_colors.dart';
import 'package:kirana_app/util/routes.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class OnBoardingScreen extends StatefulWidget {
  const OnBoardingScreen({super.key});

  @override
  State<OnBoardingScreen> createState() => _OnBoardingScreenState();
}

class _OnBoardingScreenState extends State<OnBoardingScreen> {
  final PageController _controller = PageController();
  bool onLastPage = false;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          PageView(
            controller: _controller,
            onPageChanged: (index) {
              setState(() {
                onLastPage = (index == 2);
              });
            },
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              const OnBoardingPage1(),
              const OnBoardingPage2(),
              const OnBoardingPage3(),
            ],
          ),
          Positioned(
            top: 0.68.sh,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              child: SmoothPageIndicator(
                controller: _controller,
                count: 3,
                effect: ExpandingDotsEffect(
                  dotColor: AppColors.Grey,
                  activeDotColor: AppColors.Black,
                  dotWidth: 15.w,
                  dotHeight: 15.h,
                ),
              ),
            ),
          ),
          Positioned(
            width: 1.sw,
            bottom: 0.02.sh,
            child: Container(
              padding: EdgeInsets.symmetric(horizontal: 20.w),
              // alignment: Alignment(-0.8.w, 0.68.h),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  onLastPage
                      ? TextButton(
                          style: TextButton.styleFrom(
                              fixedSize: Size(208.w, 46.h),
                              backgroundColor: AppColors.Primary),
                          onPressed: () {
                            Navigator.pushNamed(context, Routes.baseRoute);
                          },
                          // ignore: prefer_const_constructors
                          child: Text(
                            "Get Started",
                            style:
                                TextStyle(color: Colors.white, fontSize: 16.sp),
                          ),
                        )
                      : TextButton(
                          style: TextButton.styleFrom(
                              fixedSize: Size(208.w, 46.h),
                              backgroundColor: AppColors.Primary),
                          onPressed: () {
                            _controller.nextPage(
                                duration: const Duration(milliseconds: 500),
                                curve: Curves.easeIn);
                          },
                          // ignore: prefer_const_constructors
                          child: Text(
                            "Next",
                            style:
                                TextStyle(color: Colors.white, fontSize: 16.sp),
                          ),
                        ),
                  onLastPage
                      ? const Text(
                          "",
                        )
                      : GestureDetector(
                          onTap: () {
                            // _controller.jumpToPage(2);
                            Navigator.pushNamed(context, Routes.baseRoute);
                          },
                          child: Text(
                            "Skip",
                            style: TextStyle(
                                color: AppColors.Primary,
                                fontSize: 16.sp,
                                fontWeight: FontWeight.w600),
                          ),
                        ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}
