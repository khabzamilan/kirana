// ignore_for_file: non_constant_identifier_names

import 'dart:ffi';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kirana_app/model/beerModel.dart';
import 'package:kirana_app/util/beerCard.dart';

import '../constants/app_colors.dart';

class BeerPage extends StatefulWidget {
  const BeerPage({super.key});

  @override
  State<BeerPage> createState() => _BeerPageState();
}

class _BeerPageState extends State<BeerPage> {
  final dio = Dio();
  List<BeerModel> list = [];
  final baseUrl = 'https://api.punkapi.com/v2/beers';
  int page = 1;
  int per_page = 10;
  bool hasNextPage = true;
  bool isFirstLoadRunning = false;
  bool isLoadMoreRunning = false;

  void firstLoad() async {
    setState(() {
      isFirstLoadRunning = true;
    });
    try {
      final response = await dio.get("$baseUrl?page=$page&per_page=$per_page");
      setState(() {
        response.data.forEach((e) {
          list.add(BeerModel.fromJson(e));
        });
        isFirstLoadRunning = false;
      });
    } catch (err) {}
  }

  void loadMore() async {
    if (hasNextPage == true &&
        isFirstLoadRunning == false &&
        isLoadMoreRunning == false &&
        controller.position.extentAfter < 300) {
      setState(() {
        isLoadMoreRunning = true;
      });
      page += 1;
      try {
        final response =
            await dio.get("$baseUrl?page=$page&per_page=$per_page");

        if (response.data.isNotEmpty) {
          setState(() {
            response.data.forEach((e) {
              list.add(BeerModel.fromJson(e));
            });
          });
        } else {
          setState(() {
            hasNextPage = false;
          });
        }
      } catch (err) {}
      setState(() {
        isLoadMoreRunning = false;
      });
    }
  }

  late ScrollController controller;

  @override
  void initState() {
    super.initState();
    firstLoad();
    controller = ScrollController()..addListener(loadMore);
  }

  @override
  void dispose() {
    controller.removeListener(loadMore);
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70.h,
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Center(
              child: FaIcon(
            FontAwesomeIcons.arrowLeft,
            color: AppColors.Primary,
          )),
        ),
        title: Center(
          child: Text(
            "Beers",
            style: TextStyle(
              color: AppColors.Black,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        backgroundColor: Colors.transparent,
      ),
      body: isFirstLoadRunning
          ? const Center(
              child: CircularProgressIndicator(),
            )
          : Column(
              children: [
                Expanded(
                  child: ListView.builder(
                    controller: controller,
                    itemCount: list.length,
                    itemBuilder: (BuildContext context, index) {
                      return Padding(
                        padding: EdgeInsets.symmetric(horizontal: 20.0.w),
                        child: Column(
                          children: [
                            BeerCard(
                              id: list[index].id,
                              title: list[index].name,
                              description: list[index].description,
                              imageUrl: list[index].imageUrl,
                              unit: list[index].volumeModel.unit,
                              value: list[index].volumeModel.value,
                              tagline: list[index].tagline,
                            ),
                            SizedBox(height: 15.h),
                          ],
                        ),
                      );
                    },
                    // itemBuilder: (_, index) =>
                    // Card(
                    //   margin:
                    //       EdgeInsets.symmetric(vertical: 8.h, horizontal: 10.w),
                    //   child: ListTile(
                    //     title: Row(
                    //       children: [
                    //         Text(
                    //           list[index].id.toString(),
                    //         ),
                    //       ],
                    //     ),
                    //   ),
                    // ),
                  ),
                ),
                if (isLoadMoreRunning == true)
                  Padding(
                    padding: const EdgeInsets.only(top: 10, bottom: 40).h,
                    child: const Center(
                      child: CircularProgressIndicator(),
                    ),
                  ),

                //when nothing else to load

                if (hasNextPage == false)
                  Container(
                    padding: const EdgeInsets.only(top: 30, bottom: 40).h,
                    color: Colors.green[200],
                    child: const Center(
                      child: Text('You have fetched all of the content'),
                    ),
                  ),
              ],
            ),
    );
  }
}
