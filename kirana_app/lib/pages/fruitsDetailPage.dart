// ignore_for_file: must_be_immutable

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kirana_app/model/fruits.dart';
import 'package:kirana_app/model/oil.dart';
import 'package:kirana_app/util/cards.dart';
import 'package:kirana_app/constants/app_colors.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

class ModelsArguments {
  final Item? item;
  List<ItemOil> list;

  ModelsArguments({this.item, required this.list});
}

class FruitsDetailPage extends StatefulWidget {
  ModelsArguments? modelsArguments;

  FruitsDetailPage({super.key, this.modelsArguments});

  @override
  State<FruitsDetailPage> createState() => _FruitsDetailPageState();
}

class _FruitsDetailPageState extends State<FruitsDetailPage> {
  final PageController _controller = PageController();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 60.h,
          elevation: 0,
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: Center(
                child: FaIcon(
              FontAwesomeIcons.arrowLeft,
              color: AppColors.Primary,
              size: 24.w,
            )),
          ),
          actions: [
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(right: 20.0).r,
                child: Container(
                  padding: const EdgeInsets.all(7).w,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50).w,
                      color: AppColors.Primary.withOpacity(0.1)),
                  child: FaIcon(
                    FontAwesomeIcons.heart,
                    color: AppColors.Primary,
                    size: 24.w,
                  ),
                ),
              ),
            )
          ],
          backgroundColor: Colors.transparent,
        ),
        body: SingleChildScrollView(
          child: Container(
            padding: EdgeInsets.symmetric(horizontal: 20.w),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 0.45.sh,
                  width: 1.sw,
                  child: Stack(
                    children: [
                      PageView(
                        controller: _controller,
                        children: [
                          SizedBox(
                            child: Image.asset(
                              widget.modelsArguments?.item?.image ?? '',
                              fit: BoxFit.contain, height: 0.2.sh,
                              // width: 1.sh,
                            ),
                          ),
                          SizedBox(
                            child: Image.asset(
                              widget.modelsArguments?.item?.image ?? '',
                              fit: BoxFit.contain,
                            ),
                          ),
                          SizedBox(
                            child: Image.asset(
                              widget.modelsArguments?.item?.image ?? '',
                              fit: BoxFit.contain,
                            ),
                          )
                        ],
                      ),
                      Container(
                        alignment: const Alignment(-0, 1),
                        child: SmoothPageIndicator(
                          controller: _controller,
                          count: 3,
                          effect: ExpandingDotsEffect(
                            dotColor: AppColors.Grey,
                            activeDotColor: AppColors.Primary,
                            radius: 20,
                            dotHeight: 8.h,
                            dotWidth: 8.w,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(height: 20.h),
                Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
                  Text(
                    widget.modelsArguments?.item?.name ?? '',
                    style:
                        TextStyle(fontWeight: FontWeight.bold, fontSize: 18.sp),
                  ),
                  SizedBox(height: 10.h),
                  RichText(
                      text: TextSpan(
                          text: widget.modelsArguments?.item?.quantity,
                          style: TextStyle(
                              fontSize: 14.sp,
                              color: AppColors.Grey,
                              fontWeight: FontWeight.w500),
                          children: <TextSpan>[
                        TextSpan(
                            text: ", Price",
                            style: TextStyle(
                                fontSize: 14.sp, color: AppColors.Grey)),
                      ])),
                  SizedBox(height: 10.h),
                  Row(
                    // mainAxisAlignment: MainAxisAlignment.start,
                    // crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      RichText(
                          text: TextSpan(
                              text: "Rs ",
                              style: TextStyle(
                                  fontSize: 18.sp,
                                  color: AppColors.Black,
                                  fontWeight: FontWeight.bold),
                              children: <TextSpan>[
                            TextSpan(
                              text: widget.modelsArguments?.item?.price
                                  .toString(),
                              // style: TextStyle(fontSize: 18, color: Pallet.Grey),
                            ),
                            TextSpan(
                                text: " (",
                                style: TextStyle(
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w400,
                                    decoration: TextDecoration.lineThrough)),
                            TextSpan(
                                text: "Nrs ",
                                style: TextStyle(
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w400,
                                    decoration: TextDecoration.lineThrough)),
                            TextSpan(
                                text: widget.modelsArguments?.item?.oldPrice
                                    .toString(),
                                style: TextStyle(
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w400,
                                    decoration: TextDecoration.lineThrough)
                                // style: TextStyle(fontSize: 18, color: Pallet.Grey),
                                ),
                            TextSpan(
                                text: ")",
                                style: TextStyle(
                                    fontSize: 10.sp,
                                    fontWeight: FontWeight.w400,
                                    decoration: TextDecoration.lineThrough)),
                          ])),
                      SizedBox(width: 5.w),
                      Container(
                        alignment: Alignment.center,
                        width: 45.w, height: 15.h,
                        // padding: const EdgeInsets.symmetric(
                        //     vertical: 0, horizontal: 5),
                        decoration: BoxDecoration(
                            color: Colors.green,
                            borderRadius: BorderRadius.circular(2)),
                        child: Text(
                          "15% OFF",
                          style:
                              TextStyle(fontSize: 10.sp, color: Colors.white),
                        ),
                      ),
                    ],
                  ),
                  SizedBox(height: 20.h),
                  Text(
                    "Product Description",
                    style:
                        TextStyle(fontWeight: FontWeight.w600, fontSize: 16.sp),
                  ),
                  SizedBox(height: 10.h),
                  Text(
                    widget.modelsArguments?.item?.desc ?? '',
                    style: TextStyle(fontSize: 12.sp, color: AppColors.Grey),
                  ),
                  SizedBox(height: 20.h),
                  Text(
                    "Ratings & Reviews (0)",
                    style:
                        TextStyle(fontWeight: FontWeight.w600, fontSize: 16.sp),
                  ),
                  SizedBox(height: 10.h),
                  Text(
                    "No Reviews Yet.",
                    style: TextStyle(fontSize: 12.sp, color: AppColors.Grey),
                  ),
                  SizedBox(height: 20.h),
                  Text(
                    "Products by Seller",
                    style:
                        TextStyle(fontWeight: FontWeight.w600, fontSize: 16.sp),
                  ),
                  SizedBox(height: 10.h),
                  Container(
                    decoration: BoxDecoration(boxShadow: [
                      BoxShadow(
                          blurRadius: 18,
                          offset: const Offset(4, 4),
                          color: AppColors.Black.withOpacity(0.1))
                    ]),
                    height: 267.h,
                    child: ListView.builder(
                      itemCount: widget.modelsArguments?.list.length,
                      itemBuilder: (context, index) {
                        var list = widget.modelsArguments?.list[index];
                        return Row(
                          children: [
                            Cards(
                                quantity: list?.quantity ?? '',
                                price: list?.price.toString() ?? '',
                                imagePath: list?.image,
                                title: list?.name ?? '',
                                oldPrice: list?.oldPrice.toString() ?? ''),
                            SizedBox(width: 15.w)
                          ],
                        );
                      },
                      scrollDirection: Axis.horizontal,
                    ),
                  ),
                  SizedBox(height: 75.h)
                ]),
              ],
            ),
          ),
        ),
        floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
        floatingActionButton: Container(
          decoration: BoxDecoration(borderRadius: BorderRadius.circular(0)),
          child: Container(
            padding: const EdgeInsets.all(10.0).w,
            // ignore: prefer_const_constructors
            decoration: BoxDecoration(color: Colors.white, boxShadow: const [
              BoxShadow(offset: Offset(0, 8), blurRadius: 16, spreadRadius: 0)
            ]),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
                const FaIcon(FontAwesomeIcons.minus),
                Container(
                  width: 47.w,
                  height: 34.h,
                  // padding: EdgeInsets.all(5),
                  decoration: BoxDecoration(
                      border: Border.all(width: 0.5.w),
                      borderRadius: BorderRadius.circular(6).w),
                  child: Center(
                    child: Text(
                      // textAlign: TextAlign.center,
                      "1",
                      style: TextStyle(
                          fontSize: 20.sp, fontWeight: FontWeight.w700),
                    ),
                  ),
                ),
                const FaIcon(FontAwesomeIcons.plus),
                Container(
                  width: 183.w,
                  height: 42.h,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(8).w,
                    // color: Pallet.Black,
                  ),
                  child: FloatingActionButton.extended(
                      backgroundColor: AppColors.Primary,
                      shape: RoundedRectangleBorder(
                          borderRadius:
                              BorderRadius.all(const Radius.circular(8).w)),
                      onPressed: () {},
                      label: Text(
                        "Add to Cart",
                        style: TextStyle(
                            fontSize: 16.sp, fontWeight: FontWeight.w600),
                      )),
                )
              ],
            ),
          ),
        ));
  }
}
