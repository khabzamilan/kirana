// ignore_for_file: avoid_print

import 'dart:convert';
import 'dart:ffi';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:http/http.dart' as http;
import 'package:kirana_app/model/dataModel.dart';
import 'package:kirana_app/util/chapters.dart';
import '../constants/app_colors.dart';

class CartPage extends StatefulWidget {
  const CartPage({super.key});

  @override
  State<CartPage> createState() => _CartPageState();
}

class _CartPageState extends State<CartPage> {
  List<DataModel> list = [];
  final dio = Dio();
  @override
  void initState() {
    super.initState();
    _fetchData();
  }

  Future<void> _fetchData() async {
    try {
      final response = await dio.get(
          'https://api.ambition.guru/api/blogs/fetch?search=&featured=1&page=1');

      setState(() {
        response.data['data'].forEach((e) {
          list.add(DataModel.fromJson(e));
        });
      });
    } on DioError catch (e) {
      if (e.response != null) {
        print('Dio error!');
        print('STATUS: ${e.response?.statusCode}');
        print('DATA: ${e.response?.data}');
        print('HEADERS: ${e.response?.headers}');
      } else {
        print('Error sending request!');
        print(e.message);
      }
    }
  }

  // Future<void> fetchData() async {
  //   final response = await http.get(Uri.parse(
  //       'https://api.ambition.guru/api/blogs/fetch?search=&featured=1&page=1'));
  //   if (response.statusCode == 200) {
  //     setState(() {
  //       var decodedAPIData = jsonDecode(response.body);
  //       // var apiData = Data.fromJson(decodedAPIData);

  //       decodedAPIData['data'].forEach((e) {
  //         list.add(DataModel.fromJson(e));
  //       });
  //     });
  //   }
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        toolbarHeight: 70,
        elevation: 0,
        leading: GestureDetector(
          onTap: () {
            Navigator.pop(context);
          },
          child: const Center(
              child: FaIcon(
            FontAwesomeIcons.arrowLeft,
            color: AppColors.Primary,
          )),
        ),
        title: Center(
          child: Text(
            // textAlign: TextAlign.center,
            "Cart",
            style: TextStyle(
              color: AppColors.Black,
              fontSize: 16.sp,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
        backgroundColor: Colors.transparent,
      ),
      body: Container(
        padding: EdgeInsets.symmetric(horizontal: 20.w),
        decoration: BoxDecoration(boxShadow: [
          BoxShadow(
              blurRadius: 18,
              offset: const Offset(4, 4),
              color: AppColors.Black.withOpacity(0.1))
        ]),
        child: ListView.builder(
            itemCount: list.length,
            itemBuilder: (BuildContext context, index) {
              return Column(
                children: [
                  Chapters(
                    title: list[index].title,
                    description: list[index].description,
                    path: list[index].thumbnailsModel.path,
                  ),
                  SizedBox(height: 15.h),
                ],
              );
            }),
      ),
    );
  }
}
