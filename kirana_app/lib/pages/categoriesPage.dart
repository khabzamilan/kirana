import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kirana_app/onBordingScreens/onBoardingPage1.dart';
import 'package:kirana_app/onBordingScreens/onBoardingPage2.dart';
import 'package:kirana_app/onBordingScreens/onBoardingPage3.dart';

import '../constants/app_colors.dart';

class CategoriesPage extends StatefulWidget {
  const CategoriesPage({super.key});

  @override
  State<CategoriesPage> createState() => _CategoriesPageState();
}

class _CategoriesPageState extends State<CategoriesPage>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;
  @override
  void initState() {
    _tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  // @override
  // void dispose() {
  //   super.dispose();
  //   _tabController.dispose();
  // }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          toolbarHeight: 70.h,
          elevation: 0,
          leading: GestureDetector(
            onTap: () {
              Navigator.pop(context);
            },
            child: const Center(
                child: FaIcon(
              FontAwesomeIcons.arrowLeft,
              color: AppColors.Primary,
            )),
          ),
          title: Center(
            child: Text(
              // textAlign: TextAlign.center,
              "Product Categories",
              style: TextStyle(
                color: AppColors.Black,
                fontSize: 16.sp,
                fontWeight: FontWeight.w500,
              ),
            ),
          ),
          actions: [
            Align(
              alignment: Alignment.center,
              child: Padding(
                padding: const EdgeInsets.only(right: 20.0).w,
                child: Container(
                  padding: const EdgeInsets.all(7).w,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(50).w,
                      color: AppColors.Primary.withOpacity(0.1)),
                  child: FaIcon(
                    FontAwesomeIcons.plus,
                    color: AppColors.Primary,
                    size: 24.sp,
                  ),
                ),
              ),
            )
          ],
          backgroundColor: Colors.transparent,
        ),
        body: Container(
          padding: EdgeInsets.symmetric(vertical: 15.h),
          child: Column(
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 20.w),
                child: TextField(
                    decoration: InputDecoration(
                        prefixIcon: const Icon(CupertinoIcons.search),
                        prefixIconColor: AppColors.Black,
                        hintText: "Search Store",
                        hintStyle:
                            TextStyle(fontSize: 13.sp, color: AppColors.Black),
                        border: OutlineInputBorder(
                            borderSide: BorderSide.none,
                            borderRadius: BorderRadius.circular(8).w),
                        filled: true,
                        fillColor: AppColors.Primary.withOpacity(0.15))),
              ),
              // GridView.count(
              //   crossAxisCount: 2,
              //   crossAxisSpacing: 10.0,
              //   mainAxisSpacing: 10.0,
              //   shrinkWrap: true,
              //   children: List.generate(20, (index) {
              //     return Padding(
              //       padding: const EdgeInsets.all(10.0),
              //       child: Container(
              //         decoration: const BoxDecoration(
              //           // image: ,
              //           borderRadius: BorderRadius.all(
              //             Radius.circular(20.0),
              //           ),
              //         ),
              //       ),
              //     );
              //   }),
              // )
              SizedBox(
                height: 15.h,
              ),
              SizedBox(
                height: 35.h,
                child: TabBar(
                  controller: _tabController,
                  indicator: const BoxDecoration(
                    // borderRadius: BorderRadius.circular(25.0),
                    border:
                        Border(bottom: BorderSide(color: AppColors.Primary)),
                    // color: Pallet.Grey,
                  ),
                  labelColor: AppColors.Primary,
                  unselectedLabelColor: AppColors.Black,
                  // ignore: prefer_const_literals_to_create_immutables
                  tabs: [
                    const Tab(text: "Fruits"),
                    const Tab(text: "Vegetable"),
                    const Tab(text: "Dairy"),
                  ],
                ),
              ),
              Expanded(
                  child: TabBarView(
                controller: _tabController,
                // ignore: prefer_const_literals_to_create_immutables
                children: [
                  //   // ignore: prefer_const_constructors
                  //   Center(
                  //     child: const Text("this is fruits Page"),
                  //   ),
                  //   // ignore: prefer_const_constructors
                  //   Center(
                  //     child: const Text("this is Vegitable Page"),
                  //   ),
                  //   const Center(
                  //     child: Text("this is Dairy Page"),
                  //   ),
                  OnBoardingPage1(),
                  OnBoardingPage2(),
                  OnBoardingPage3(),
                ],
              ))
            ],
          ),
        ));
  }
}
