class DataModel {
  final int id;
  final String title;
  final String slug;
  final String description;
  final String createdAt;
  final String metaTitle;
  final String metaKeyWords;
  final String metaDescription;
  final DataCategoryModel categoryModel;
  final DataThumbnailsModel thumbnailsModel;
  final DataThumbnailsModel featuresModel;
  final DataAdminModel adminModel;

  DataModel({
    required this.id,
    required this.title,
    required this.slug,
    required this.description,
    required this.createdAt,
    required this.metaTitle,
    required this.metaKeyWords,
    required this.metaDescription,
    required this.categoryModel,
    required this.adminModel,
    required this.thumbnailsModel,
    required this.featuresModel,
  });

  factory DataModel.fromJson(Map<String, dynamic> json) {
    return DataModel(
      id: json['id'],
      title: json['title'],
      slug: json['slug'],
      description: json['description'],
      createdAt: json['created_at'],
      metaTitle: json['meta_title'],
      metaKeyWords: json['meta_keywords'],
      metaDescription: json['meta_description'],
      categoryModel: DataCategoryModel.fromJson(json['category']),
      thumbnailsModel: DataThumbnailsModel.fromJson(json['thumbnails']),
      featuresModel: DataThumbnailsModel.fromJson(json['features']),
      adminModel: DataAdminModel.fromJson(json['admin']),
    );
  }
}

class DataCategoryModel {
  final int id;
  final String name;
  final String slug;
  final String description;
  final dynamic extras;
  final String createdAt;
  final String updatedAt;
  final dynamic deletedAt;

  DataCategoryModel({
    required this.id,
    required this.name,
    required this.slug,
    required this.description,
    this.extras,
    required this.createdAt,
    required this.updatedAt,
    this.deletedAt,
  });

  factory DataCategoryModel.fromJson(Map<String, dynamic> json) {
    return DataCategoryModel(
        id: json['id'],
        name: json['name'],
        slug: json['slug'],
        description: json['description'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at']);
  }
}

class DataThumbnailsModel {
  final String path;
  final dynamic variant;
  final dynamic variantName;
  final String finename;
  final int size;
  final String extension;
  final int id;
  final String aggregateType;
  final int duration;

  DataThumbnailsModel({
    required this.path,
    this.variant,
    this.variantName,
    required this.finename,
    required this.size,
    required this.extension,
    required this.id,
    required this.aggregateType,
    required this.duration,
  });

  factory DataThumbnailsModel.fromJson(Map<String, dynamic> json) {
    return DataThumbnailsModel(
        path: json['path'],
        finename: json['filename'],
        size: json['size'],
        extension: json['extension'],
        id: json['id'],
        aggregateType: json['aggregate_type'],
        duration: json['duration']);
  }
}

class DataAdminModel {
  final String name;
  final dynamic symbol;
  final int id;

  DataAdminModel({
    required this.name,
    required this.id,
    this.symbol,
  });

  factory DataAdminModel.fromJson(Map<String, dynamic> json) {
    return DataAdminModel(
      name: json['name'],
      id: json['id'],
    );
  }
}
