class FruitsModel {
  static List<Item> items = [
    // Item(
    //   id: 1,
    //   name: "Apple",
    //   desc: "abfg sdfgkds,gn dskfndsfklsadf kdsg dsflkdslkf",
    //   price: 300,
    //   image: "asset/images/apple.png",
    // )
  ];
}

class Item {
  final int id;
  final String name;
  final String desc;
  final num price;
  final String? image;
  int count;
  final String? quantity;
  final num? oldPrice;

  Item({
    required this.id,
    required this.name,
    required this.desc,
    required this.price,
    required this.image,
    this.count = 0,
    required this.quantity,
    required this.oldPrice,
  });

  factory Item.fromMap(Map<String, dynamic> map) {
    return Item(
        id: map["id"],
        name: map["name"],
        desc: map["desc"],
        price: map["price"],
        image: map["image"],
        quantity: map["quantity"],
        oldPrice: map["oldPrice"]);
  }

  toMap() => {
        "id": id,
        "name": name,
        "desc": desc,
        "price": price,
        "image": image,
        "quantity": quantity,
        "oldPrice": oldPrice
      };
}
