class ShoppingModel {
  String? id;
  String? shoppingList;
  bool isDone;

  ShoppingModel(
      {required this.id, required this.shoppingList, this.isDone = false});

  static List<ShoppingModel> itemList() {
    return [
      ShoppingModel(id: '01', shoppingList: 'Banana', isDone: true),
      ShoppingModel(id: '02', shoppingList: 'Tomato', isDone: true),
      ShoppingModel(id: '03', shoppingList: 'Banana'),
      ShoppingModel(id: '04', shoppingList: 'Dragon Fruit'),
      ShoppingModel(id: '05', shoppingList: 'Grapes'),
      ShoppingModel(id: '06', shoppingList: 'Carrot'),
    ];
  }
}
