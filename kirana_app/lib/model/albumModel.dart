class AlbumModel {
  static List<Album> albums = [];
}

class Album {
  final int? userId;
  final int? id;
  final String title;
  final String body;

  Album({this.userId, this.id, required this.title, required this.body});

  factory Album.fromJson(Map<String, dynamic> json) {
    return Album(
        userId: json['userId'],
        id: json['id'],
        title: json['title'],
        body: json['body']);
  }
  // toMap() => {
  //       'userId': userId,
  //       'id': id,
  //       'title': title,
  //     };
}
