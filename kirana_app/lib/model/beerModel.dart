class BeerModel {
  final int id;
  final String name;
  final String description;
  final String imageUrl;
  final String tagline;

  final VolumeModel volumeModel;

  BeerModel({
    required this.tagline,
    required this.volumeModel,
    required this.id,
    required this.name,
    required this.description,
    required this.imageUrl,
  });
  factory BeerModel.fromJson(Map<String, dynamic> json) {
    return BeerModel(
        volumeModel: VolumeModel.fromJson(json['volume']),
        id: json['id'],
        name: json['name'],
        description: json['description'],
        imageUrl: json['image_url'],
        tagline: json['tagline']);
  }
}

class VolumeModel {
  final int value;
  final String unit;

  VolumeModel({required this.unit, required this.value});
  factory VolumeModel.fromJson(Map<String, dynamic> json) {
    return VolumeModel(unit: json['unit'], value: json['value']);
  }
}
