import 'dart:io';

import 'package:flutter/material.dart';
import 'package:kirana_app/main.dart';
import 'package:kirana_app/pages/allFruitsPage.dart';
import 'package:kirana_app/pages/beerPage.dart';
import 'package:kirana_app/pages/categoriesPage.dart';
import 'package:kirana_app/pages/cookingOilDetailPage.dart';
import 'package:kirana_app/pages/fruitsDetailPage.dart';
import 'package:kirana_app/pages/imagepicker.dart';
import 'package:kirana_app/util/bottomNavPage.dart';
import 'package:kirana_app/util/routes.dart';

class RouteGenerator {
  static Route<dynamic> generateRoutes(RouteSettings settings) {
    var arguments = settings.arguments;

    switch (settings.name) {
      case Routes.fruitdetailPage:
        return MaterialPageRoute(
            builder: (context) =>
                // FruitsDetailPage(models: arguments as Models));
                FruitsDetailPage(
                  modelsArguments: arguments as ModelsArguments?,
                ));

      case Routes.cookingoildetailPage:
        return MaterialPageRoute(
            builder: (context) =>
                // FruitsDetailPage(models: arguments as Models));
                CookingOilDetailPage(
                  cookingOilModelsArguments:
                      arguments as CookingOilModelsArguments?,
                ));
      case Routes.splashRoute:
        return MaterialPageRoute(builder: (context) => const SplashScreen());
      case Routes.baseRoute:
        return MaterialPageRoute(builder: (context) => const BaseScreen());
      case Routes.categoriesRoute:
        return MaterialPageRoute(builder: (context) => const CategoriesPage());

      case Routes.imagepicker:
        return MaterialPageRoute(
            builder: (context) =>
                ImagePickerWidgetScreen(onImageSelected: (File file) {}));

      case Routes.allFruitsRoute:
        return MaterialPageRoute(builder: (context) => const AllFruitPage());
      case Routes.beerPage:
        return MaterialPageRoute(builder: (context) => const BeerPage());
      default:
        return MaterialPageRoute(builder: (context) => const BaseScreen());
    }
  }
}
