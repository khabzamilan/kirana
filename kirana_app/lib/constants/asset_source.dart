class AssetSource {
  static const String appLogo = 'asset/images/logo.png';
  // static const String appLogo = 'asset/images/logo.png';
  // static const String appLogo = 'asset/images/logo.png';

  //banner images
  static const String mainBanner = 'asset/images/promotion.png';
  static const String promotionalBanner1 = 'asset/images/promo1.png';
  static const String promotionalBanner2 = 'asset/images/promo2.png';
  static const String promotionalBanner3 = 'asset/images/promo3.png';

  //category images
  static const String vegetablesFruits = 'asset/images/fruits.png';
  static const String bakery = 'asset/images/bread.png';
  static const String dairyEggs = 'asset/images/dairy.png';
  static const String meatFish = 'asset/images/fish.png';
  static const String beautyProducts = 'asset/images/shampoo.png';

  //Fruits images
  static const String appleImage = 'asset/images/apple.png';
}
