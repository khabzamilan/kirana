// ignore_for_file: constant_identifier_names, camel_case_types

import 'package:flutter/material.dart';

class AppColors {
  //light theme color code
  static const Color Primary = Color.fromRGBO(183, 34, 41, 1);
  static const Color Black = Color.fromRGBO(22, 4, 5, 1);
  static const Color Grey = Color.fromRGBO(157, 157, 157, 1);
}
