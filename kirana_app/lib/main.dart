import 'dart:async';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import 'package:kirana_app/pages/onboardingPage.dart';
import 'package:kirana_app/route_generator.dart';

import 'package:kirana_app/constants/app_colors.dart';
import 'package:kirana_app/util/routes.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ScreenUtilInit(
      designSize: const Size(360, 640),
      builder: (BuildContext context, Widget? child) {
        return MaterialApp(
          debugShowCheckedModeBanner: false,
          theme: ThemeData(fontFamily: 'Poppins'),
          // home: SplashScreen(),
          initialRoute: Routes.splashRoute,
          onGenerateRoute: RouteGenerator.generateRoutes,
        );
      },
    );
  }
}

class SplashScreen extends StatefulWidget {
  const SplashScreen({super.key});

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {
  @override
  void initState() {
    super.initState();
    Timer(
        const Duration(seconds: 4),
        () => Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => const OnBoardingScreen())));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Stack(
      fit: StackFit.expand,
      children: [
        Container(
          decoration: const BoxDecoration(color: AppColors.Primary),
        ),
        Column(mainAxisAlignment: MainAxisAlignment.center, children: [
          Expanded(
              flex: 2,
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Image.asset("asset/images/logo.png"),
                  const Text(
                    "KIRANA",
                    style: TextStyle(
                        color: Colors.white,
                        fontSize: 40,
                        fontWeight: FontWeight.bold),
                  ),
                ],
              )),
          Expanded(
            flex: 1,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              // ignore: prefer_const_literals_to_create_immutables
              children: [
                const SizedBox(height: 20),
                const CircularProgressIndicator(),
                const Padding(
                  padding: EdgeInsetsDirectional.only(top: 20.0),
                ),
                const Text(
                  textAlign: TextAlign.center,
                  'Online Grocery Store \nFor Everyone',
                  style: TextStyle(color: Colors.white, fontSize: 18),
                )
              ],
            ),
          )
        ])
      ],
    ));
  }
}
