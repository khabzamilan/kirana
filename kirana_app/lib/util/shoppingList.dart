// ignore_for_file: prefer_typing_uninitialized_variables, file_names

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kirana_app/constants/app_colors.dart';

import '../model/shoppingListModel.dart';

class ShoppingList extends StatelessWidget {
  final ShoppingModel shoppingModel;
  final onToDoChanged;
  final onDeleteItem;
  const ShoppingList(
      {super.key,
      required this.shoppingModel,
      required this.onToDoChanged,
      required this.onDeleteItem});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(8).w,
          side: BorderSide(
              color: AppColors.Primary.withOpacity(0.2), width: 0.5.w)),
      elevation: 10,
      shadowColor: AppColors.Black.withOpacity(0.25),
      child: ListTile(
        onTap: () {
          onToDoChanged(shoppingModel);
        },
        contentPadding: EdgeInsets.symmetric(horizontal: 15.w, vertical: 5.h),
        leading: Icon(
          shoppingModel.isDone ? Icons.check_circle : Icons.circle_outlined,
          color: AppColors.Primary,
        ),
        title: Text(
          shoppingModel.shoppingList!,
          style: TextStyle(
              decoration:
                  shoppingModel.isDone ? TextDecoration.lineThrough : null,
              color: AppColors.Grey,
              fontSize: 16.sp,
              fontWeight: FontWeight.w500),
        ),
        trailing: Container(
          height: 35.h,
          width: 35.w,
          decoration: BoxDecoration(
              color: AppColors.Primary,
              borderRadius: BorderRadius.circular(6).w),
          child: IconButton(
            color: Colors.white,
            iconSize: 18.w,
            icon: const Icon(Icons.delete),
            onPressed: () {
              onDeleteItem(shoppingModel.id);
            },
          ),
        ),
      ),
    );
  }
}
