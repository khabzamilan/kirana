import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:kirana_app/constants/app_colors.dart';

class CategoriesCard extends StatelessWidget {
  // ignore: prefer_typing_uninitialized_variables
  final imagePath;
  final String categoryName;
  const CategoriesCard({super.key, this.imagePath, required this.categoryName});

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 80.w,
      height: 90.h,
      child: Column(
        children: [
          SizedBox(
            height: 54.h,
            child: Image.asset(imagePath),
          ),
          Text(
            categoryName,
            textAlign: TextAlign.center,
            style: TextStyle(
              fontSize: 13.sp,
              color: AppColors.Black,
            ),
          )
        ],
      ),
    );
  }
}
