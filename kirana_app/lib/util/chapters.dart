import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../constants/app_colors.dart';

class Chapters extends StatefulWidget {
  final String title;
  final String description;
  final String path;
  const Chapters({
    super.key,
    required this.title,
    required this.description,
    required this.path,
  });

  @override
  State<Chapters> createState() => _ChaptersState();
}

class _ChaptersState extends State<Chapters> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.all(10.0).w,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8).w,
        border: Border.all(width: 1.w, color: AppColors.Grey.withOpacity(0.3)),
      ),
      child: Column(children: [
        Text(
          widget.title,
          style: TextStyle(
              fontSize: 16.sp,
              color: AppColors.Primary,
              fontWeight: FontWeight.w500),
        ),
        SizedBox(height: 5.h),
        Image.network(
          widget.path,
          height: 185.h,
          width: 370.w,
        ),
        SizedBox(height: 5.h),
        Text(
          widget.description,
          style: TextStyle(fontSize: 13.sp, color: AppColors.Grey),
        )
      ]),
    );
  }
}
