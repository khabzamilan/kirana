// ignore_for_file: file_names, prefer_typing_uninitialized_variables

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:kirana_app/constants/app_colors.dart';

class Cards extends StatefulWidget {
  final String oldPrice;
  final String price;
  final imagePath;
  final String title;
  int count;
  final String quantity;
  // final Item item;
  Cards(
      {super.key,
      required this.quantity,
      required this.price,
      this.count = 0,
      required this.imagePath,
      required this.title,
      required this.oldPrice}) {
    // assert(item != null);
  }

  @override
  State<Cards> createState() => _CardsState();
}

class _CardsState extends State<Cards> {
  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(vertical: 15.h, horizontal: 10.w),
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8).w,
        border: Border.all(width: 1.w, color: AppColors.Grey.withOpacity(0.3)),
      ),
      width: 175.w,
      child: Column(
        children: [
          SizedBox(
            height: 90.h,
            width: 155.w,
            child: Image.asset(widget.imagePath),
          ),
          SizedBox(height: 10.h),
          // ignore: prefer_const_constructors
          Align(
            alignment: Alignment.topLeft,
            child: SizedBox(
              height: 45.h,
              child: Text(
                widget.title,
                textAlign: TextAlign.start,
                style: TextStyle(
                    fontSize: 15.sp,
                    color: AppColors.Black,
                    fontWeight: FontWeight.w500),
              ),
            ),
          ),
          SizedBox(height: 5.h),

          Align(
            alignment: Alignment.centerLeft,
            child: RichText(
                text: TextSpan(
                    text: widget.quantity,
                    style: TextStyle(color: AppColors.Grey, fontSize: 13.sp),
                    children: const <TextSpan>[TextSpan(text: ', Price')])),
          ),
          SizedBox(height: 8.h),
          Align(
            alignment: Alignment.centerLeft,
            child: RichText(
                text: TextSpan(
                    text: "NRs. ",
                    style: TextStyle(color: AppColors.Black, fontSize: 12.sp),
                    children: <TextSpan>[
                  TextSpan(
                      text: widget.price,
                      style: TextStyle(
                          fontSize: 15.sp, fontWeight: FontWeight.w600)),
                  TextSpan(
                      text: ' Nrs. ',
                      style: TextStyle(
                          fontSize: 10.sp,
                          fontWeight: FontWeight.w400,
                          decoration: TextDecoration.lineThrough)),
                  TextSpan(
                      text: widget.oldPrice,
                      style: TextStyle(
                          fontSize: 10.sp,
                          fontWeight: FontWeight.w400,
                          decoration: TextDecoration.lineThrough)),
                ])),
          ),
          SizedBox(height: 15.h),

          Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            // ignore: prefer_const_literals_to_create_immutables
            children: [
              Container(
                // alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2).w,
                    border: Border.all(
                      width: 1.w,
                      color: AppColors.Primary,
                    )),
                height: 24.h,
                width: 24.w,
                child: IconButton(
                  // alignment: Alignment.center,
                  padding: const EdgeInsets.all(0),
                  onPressed: () {
                    setState(() {
                      widget.count--;
                    });
                  },
                  icon: FaIcon(FontAwesomeIcons.minus,
                      color: AppColors.Primary, size: 15.w),
                ),
              ),
              Text(
                widget.count.toString(),
                style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.w600),
              ),
              Container(
                // alignment: Alignment.center,
                decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(2),
                    border: Border.all(
                      width: 1.w,
                      color: AppColors.Primary,
                    )),
                height: 24.h,
                width: 24.w,
                child: IconButton(
                  // alignment: Alignment.center,
                  padding: const EdgeInsets.all(0),
                  onPressed: () {
                    setState(() {
                      widget.count++;
                    });
                  },
                  icon: FaIcon(FontAwesomeIcons.plus,
                      color: AppColors.Primary, size: 15.sp),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }
}
