import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../constants/app_colors.dart';

class BeerCard extends StatefulWidget {
  final int id;
  final String title;
  final String description;
  final String imageUrl;
  final int value;
  final String unit;
  final String tagline;
  const BeerCard({
    super.key,
    required this.tagline,
    required this.value,
    required this.unit,
    required this.id,
    required this.title,
    required this.description,
    required this.imageUrl,
  });

  @override
  State<BeerCard> createState() => _BeerCardState();
}

class _BeerCardState extends State<BeerCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
      // width: 170.w,
      padding: const EdgeInsets.all(10.0).w,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(8).w,
        border: Border.all(width: 1.w, color: AppColors.Grey.withOpacity(0.3)),
      ),
      child: Column(children: [
        Row(
          children: [
            Text(widget.id.toString(),
                style: TextStyle(fontSize: 18.sp, fontWeight: FontWeight.w500)),
            SizedBox(width: 10.w),
            Text(
              widget.title,
              style: TextStyle(
                  fontSize: 18.sp,
                  color: AppColors.Black,
                  fontWeight: FontWeight.w500),
            ),
          ],
        ),
        SizedBox(height: 10.h),
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            RichText(
                text: TextSpan(
                    text: "\"",
                    style: TextStyle(color: AppColors.Primary, fontSize: 16.sp),
                    children: <TextSpan>[
                  TextSpan(text: widget.tagline),
                  const TextSpan(text: "\"")
                ])),
            // Text(widget.tagline)
          ],
        ),
        SizedBox(height: 15.h),
        Image.network(
          widget.imageUrl,
          height: 185.h,
          width: 370.w,
        ),
        SizedBox(height: 10.h),
        Row(
          children: [
            Text(widget.value.toString()),
            SizedBox(width: 2.w),
            Text(widget.unit),
          ],
        ),
        SizedBox(height: 10.h),
        Text(
          widget.description,
          style: TextStyle(fontSize: 13.sp, color: AppColors.Grey),
        )
      ]),
    );
  }
}
