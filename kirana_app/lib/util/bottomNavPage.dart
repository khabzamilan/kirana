import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:kirana_app/constants/custom_icon.dart';
import 'package:kirana_app/onBordingScreens/onBoardingPage1.dart';
import 'package:kirana_app/onBordingScreens/onBoardingPage2.dart';
import 'package:kirana_app/onBordingScreens/onBoardingPage3.dart';
import 'package:kirana_app/pages/cartPage.dart';
import 'package:kirana_app/pages/chatPage.dart';
import 'package:kirana_app/pages/homePage.dart';
import 'package:kirana_app/pages/listPage.dart';
import 'package:kirana_app/pages/profilePage.dart';
import 'package:kirana_app/constants/app_colors.dart';

class BaseScreen extends StatefulWidget {
  const BaseScreen({super.key});

  @override
  State<BaseScreen> createState() => _BaseScreenState();
}

class _BaseScreenState extends State<BaseScreen> {
  int _selectedIndex = 0;

  void _onItemTapped(int index) {
    setState(() {
      _selectedIndex = index;
    });
  }

  List<Widget> list = [
    const HomePage(),
    const ListPage(),
    const ChatPage(),
    const CartPage(),
    const ProfilePage()
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: list[_selectedIndex],
      bottomNavigationBar: SizedBox(
        height: 80.h,
        child: BottomNavigationBar(
          type: BottomNavigationBarType.fixed,
          items: [
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  CustomIcon.shopIcon,
                  height: 24.h,
                  width: 24.w,
                  colorFilter:
                      const ColorFilter.mode(AppColors.Grey, BlendMode.srcIn),
                ),
                label: 'Home',
                activeIcon: SvgPicture.asset(
                  CustomIcon.shopIcon,
                  height: 26.h,
                  width: 26.w,
                  colorFilter: const ColorFilter.mode(
                      AppColors.Primary, BlendMode.srcIn),
                )),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  CustomIcon.listIcon,
                  height: 24.h,
                  width: 24.w,
                ),
                label: 'List',
                activeIcon: SvgPicture.asset(
                  CustomIcon.listIcon,
                  height: 26.h,
                  width: 26.w,
                  colorFilter: const ColorFilter.mode(
                      AppColors.Primary, BlendMode.srcIn),
                )),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  CustomIcon.chatIcon,
                  height: 24.h,
                  width: 24.w,
                ),
                label: 'Chat',
                activeIcon: SvgPicture.asset(
                  CustomIcon.chatIcon,
                  height: 26.h,
                  width: 26.w,
                  colorFilter: const ColorFilter.mode(
                      AppColors.Primary, BlendMode.srcIn),
                )),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  CustomIcon.cartIcon,
                  height: 24.h,
                  width: 24.w,
                ),
                label: 'Cart',
                activeIcon: SvgPicture.asset(
                  CustomIcon.cartIcon,
                  height: 26.h,
                  width: 26.w,
                  colorFilter: const ColorFilter.mode(
                      AppColors.Primary, BlendMode.srcIn),
                )),
            BottomNavigationBarItem(
                icon: SvgPicture.asset(
                  CustomIcon.profileIcon,
                  height: 24.h,
                  width: 24.w,
                ),
                label: 'Profile',
                activeIcon: SvgPicture.asset(
                  CustomIcon.profileIcon,
                  height: 26.h,
                  width: 26.w,
                  colorFilter: const ColorFilter.mode(
                      AppColors.Primary, BlendMode.srcIn),
                )),
          ],
          currentIndex: _selectedIndex,
          selectedItemColor: AppColors.Primary,
          selectedFontSize: 13.sp,
          unselectedFontSize: 12.sp,
          // selectedIconTheme:
          //     const IconThemeData(color: Pallet.Primary, size: 24),
          // unselectedIconTheme: const IconThemeData(color: Pallet.Grey),
          showSelectedLabels: true,
          onTap: _onItemTapped,
          showUnselectedLabels: true,
        ),
      ),
    );
  }
}
