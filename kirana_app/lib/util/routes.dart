class Routes {
  static const String listPageRoute = "/lastPage";
  static const String fruitdetailPage = "/fruitdetailPage";
  static const String cookingoildetailPage = "/cookingoildetailPage";
  static const String categoriesRoute = "/categoriesPage";
  static const String baseRoute = "/homePage";
  static const String splashRoute = "/";
  static const String allFruitsRoute = "/allFruitsPage";
  static const String beerPage = "/beerPage";
  static const String imagepicker = "/imagePicker";
}
