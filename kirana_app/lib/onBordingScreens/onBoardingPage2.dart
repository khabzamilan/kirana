// ignore_for_file: file_names

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';

import '../constants/app_colors.dart';

class OnBoardingPage2 extends StatelessWidget {
  const OnBoardingPage2({super.key});

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: SingleChildScrollView(
        child: Container(
            padding: const EdgeInsets.only(top: 70).r,
            child: Column(
              children: [
                SizedBox(
                  child: Image.asset(
                    "asset/images/onb2.png",
                    height: 0.5.sh,
                  ),
                ),
                Container(
                  padding:
                      EdgeInsets.symmetric(vertical: 52.h, horizontal: 20.w),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    // ignore: prefer_const_literals_to_create_immutables
                    children: [
                      Text(
                        "Hassle Free Payment",
                        style: TextStyle(
                            fontSize: 24.sp,
                            fontWeight: FontWeight.w500,
                            color: AppColors.Black),
                      ),
                      SizedBox(height: 5.h),
                      Text(
                        "Pay as per your convenience, we accept all credit, debit cards and e-wallets.",
                        style:
                            TextStyle(fontSize: 15.sp, color: AppColors.Grey),
                      ),
                    ],
                  ),
                ),
              ],
            )),
      ),
      // color: Colors.red,
    );
  }
}
